API PoC install
===
```
vagrant up
vagrant ssh
cd /vagrant
composer install
```
Start worker
===
```
app/console rabbitmq:consumer ingest_media
```
It shall pool ingest API every 3 minutes (*/3 * * * *)

API
===

http://192.168.66.6/media

RabbitMq
===

http://192.168.66.6:15672
login: guest
password: guest