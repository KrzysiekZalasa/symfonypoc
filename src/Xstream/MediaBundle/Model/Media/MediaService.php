<?php
namespace Xstream\MediaBundle\Model\Media;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Xstream\MediaBundle\Entity\Media;
use Xstream\MediaBundle\Entity\MediaRepository;

class MediaService
{
    /**
     * @var MediaRepository
     */
    protected $mediaRepository;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @param MediaRepository $mediaRepository
     * @param ValidatorInterface $validator
     */
    public function __construct(MediaRepository $mediaRepository, ValidatorInterface $validator)
    {
        $this->mediaRepository = $mediaRepository;
        $this->validator = $validator;
    }

    /**
     * Adds single media to media library
     *
     * @param Media $media
     */
    public function addMediaToLibrary(Media $media)
    {
        $errors = $this->validator->validate($media);
        if (count($errors) > 0) {
            throw new ValidatorException($errors);
        }

        $this->mediaRepository->persist($media);
    }

    /**
     * Removes single media from library
     *
     * @param $mediaId
     */
    public function removeMediaFromLibrary($mediaId)
    {
        $media = $this->readMediaFromLibrary($mediaId);

        $this->mediaRepository->remove($media);
    }

    /**
     * Updates media in library - it supports only title and description
     *
     * @param UpdateMediaCommand $updateMediaCommand
     */
    public function updateMediaInLibrary(UpdateMediaCommand $updateMediaCommand)
    {
        $media = $this->readMediaFromLibrary($updateMediaCommand->getId());

        $media->setTitle($updateMediaCommand->getTitle());
        $media->setDescription($updateMediaCommand->getDescription());

        $errors = $this->validator->validate($media);
        if (count($errors) > 0) {
            throw new ValidatorException($errors);
        }

        $this->mediaRepository->persist($media);
    }

    /**
     * Reads one media from media library
     *
     * @param $mediaId
     * @return Media
     */
    public function readMediaFromLibrary($mediaId)
    {
        $media = $this->mediaRepository->find($mediaId);
        if (is_null($media)) {
            throw new NotFoundHttpException("Media doesn't exist");
        }

        return $media;
    }

    /**
     * Search in Media Library against SearchMediaQuery
     *
     * @param SearchMediaQuery $searchMediaQuery
     *
     * @return \Xstream\MediaBundle\Entity\MediaLibrary
     */
    public function searchMediaInLibrary(SearchMediaQuery $searchMediaQuery)
    {
        $this->validate($searchMediaQuery);

        return $this->mediaRepository->search($searchMediaQuery);
    }

    /**
     * Validates provided object
     *
     * @param $object
     */
    protected function validate($object)
    {
        $errors = $this->validator->validate($object);
        if (count($errors) > 0) {
            throw new ValidatorException($errors);
        }
    }
}