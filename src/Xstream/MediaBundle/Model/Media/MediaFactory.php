<?php
namespace Xstream\MediaBundle\Model\Media;

use Symfony\Component\PropertyAccess\PropertyAccess;
use Xstream\MediaBundle\Entity\Image;
use Xstream\MediaBundle\Entity\Media;
use Xstream\MediaBundle\Entity\Stream;

class MediaFactory
{
    /**
     * Set up required libraries
     */
    public function __construct()
    {
        $this->accessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * Creates Media from Media deserialized by JMS serializer
     *
     * Deserialized media doesn't have relations in child objects which breaks doctrine serialization     *
     *
     * @param Media $media
     *
     * @return Media
     */
    public function createMediaFromDeserializedMedia(Media $media)
    {
        if (count($media->getImages()) > 0) {
            foreach ($media->getImages() as $image) {
                $image->setMedia($media);
            }
        }

        if (count($media->getStreams()) > 0) {
            foreach ($media->getStreams() as $stream) {
                $stream->setMedia($media);
            }
        }

        return $media;
    }

    /**
     * Builds single Media just from plain Djament single object
     *
     * @param array $djament
     * @return Media
     */
    public function createMediaFromRawDjament(array $djament)
    {
        $media = new Media();
        $media->setTitle($this->accessor->getValue($djament, '[titles][default]'));
        $media->setDescription($this->accessor->getValue($djament, '[descriptions][default]'));
        $media->setExternalId($this->accessor->getValue($djament, '[external_id]'));
        $media->setImages($this->createMediaImagesFromRawDjament($media, $djament));
        $media->setStreams($this->createMediaStreamsFromRawDjament($media, $djament));

        return $media;
    }

    /**
     * Build collection of media from full Djament list
     *
     * @param array $djament
     * @return array
     */
    public function createMediaListFromRawDjamentList(array $djament)
    {
        $rawDjamentList = $this->accessor->getValue($djament, '[list]');
        $mediaList = [];

        foreach ($rawDjamentList as $rawDjament) {
            $mediaList[] = $this->createMediaFromRawDjament($rawDjament);
        }

        return $mediaList;
    }

    /**
     * Creates collection of Images from plain Djament single object
     *
     * @param Media $media Required for proper relation mapping
     * @param array $djament Single media in Djament
     *
     * @return array<Image>
     */
    protected function createMediaImagesFromRawDjament(Media $media, array $djament)
    {
        $images = [];

        $djamentImages = $this->accessor->getValue($djament, '[images]');
        foreach ($djamentImages as $djamentImage) {
            $image = new Image();
            $image->setSrc($this->accessor->getValue($djamentImage, '[formats][original_size][src]'));
            $image->setWidth($this->accessor->getValue($djamentImage, '[formats][original_size][width]'));
            $image->setHeight($this->accessor->getValue($djamentImage, '[formats][original_size][height]'));
            $image->setMedia($media);

            $images[] = $image;
        }

        return $images;
    }

    /**
     * Created collection of Streams from plain Djament single object
     *
     * @param Media $media Required for proper relation mapping
     * @param array $djament Single media in Djament
     *
     * @return array<Stream>
     */
    protected function createMediaStreamsFromRawDjament(Media $media, array $djament)
    {
        $streams = [];

        $djamentStreams = $this->accessor->getValue($djament, '[streams][wvm]');
        foreach ($djamentStreams as $djamentStream) {
            $stream = new Stream();
            $stream->setSrc($this->accessor->getValue($djamentStream, '[src]'));
            $stream->setType($this->accessor->getValue($djamentStream, '[type]'));
            $stream->setMedia($media);

            $streams[] = $stream;
        }

        return $streams;
    }
}