<?php
namespace Xstream\MediaBundle\Model\Media;

use GuzzleHttp\Client;
use Symfony\Component\Validator\Exception\ValidatorException;

class MediaIngestService
{
    /**
     * @var Client
     */
    protected $httpClient;

    /**
     * @var MediaService
     */
    protected $mediaService;

    /**
     * @var MediaFactory
     */
    protected $mediaFactory;

    /**
     * @param Client $httpClient
     * @param MediaService $mediaService
     */
    public function __construct(Client $httpClient, MediaService $mediaService)
    {
        $this->httpClient = $httpClient;
        $this->mediaService = $mediaService;
        $this->mediaFactory = new MediaFactory();
    }

    /**
     * Imports Media from Ingest API
     *
     * Media has to meet all domain requirements ex. unique externalId or presence of title
     *
     * Incorrect entries are just omitted because of limited PoC scope
     */
    public function importIngestedMedia()
    {
        $result = $this->httpClient->get('media/videos');

        $response = json_decode($result->getBody(), true);
        $mediaList = $this->mediaFactory->createMediaListFromRawDjamentList($response);

        foreach ($mediaList as $media) {
            try {
                $this->mediaService->addMediaToLibrary($media);
            } catch (ValidatorException $e) {
                // do nothing - it's PoC only :)
            }
        }
    }
}