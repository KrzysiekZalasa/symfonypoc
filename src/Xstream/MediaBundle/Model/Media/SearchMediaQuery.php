<?php
namespace Xstream\MediaBundle\Model\Media;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Exception\ValidatorException;

class SearchMediaQuery
{
    /**
     * If sort param is provided then order param has to be defined
     * For null sort param order has to be also null
     *
     * @Serializer\Type("string")
     * @Assert\Choice(choices= {"title", "id"})
     * @Assert\Expression("(this.getSort() !== null and this.getOrder() !== null) or (this.getSort() === null and this.getOrder() === null)", message="sort parameter requires proper order param")
     */
    protected $sort;

    /**
     * @Serializer\Type("string")
     * @Assert\Choice(choices= {"asc", "desc"})
     */
    protected $order;

    /**
     * @Serializer\Type("integer")
     * @Assert\Range(min = 1, max = 20)
     */
    protected $limit;

    /**
     * @Serializer\Type("integer")
     * @Assert\Type("numeric")
     * @Assert\GreaterThanOrEqual(0)
     */
    protected $offset;

    public function __construct(array $params)
    {
        $this->setSort($params['sort']);
        $this->setOrder($params['order']);
        $this->setLimit($params['limit']);
        $this->setOffset($params['offset']);
    }

    /**
     * @return mixed
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param mixed $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param mixed $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return mixed
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param mixed $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }


}