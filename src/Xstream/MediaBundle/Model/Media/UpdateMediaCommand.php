<?php
namespace Xstream\MediaBundle\Model\Media;

use JMS\Serializer\Annotation as Serializer;

class UpdateMediaCommand
{
    /**
     * @Serializer\Type("integer")
     */
    protected $id;

    /**
     * @Serializer\Type("string")
     */
    protected $title;

    /**
     * @Serializer\Type("string")
     */
    protected $description;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }


}