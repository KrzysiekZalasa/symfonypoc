<?php
namespace Xstream\MediaBundle\Consumer;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Xstream\MediaBundle\Model\Media\MediaIngestService;

class MediaIngestConsumer implements ConsumerInterface
{
    /**
     * @var MediaIngestService
     */
    protected $mediaIngestService;

    /**
     * @param MediaIngestService $mediaIngestService
     */
    public function __construct(MediaIngestService $mediaIngestService)
    {
        $this->mediaIngestService = $mediaIngestService;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(AMQPMessage $msg)
    {
        $this->mediaIngestService->importIngestedMedia();

        return true;
    }
}