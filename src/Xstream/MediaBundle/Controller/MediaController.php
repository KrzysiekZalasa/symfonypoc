<?php
namespace Xstream\MediaBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\View;
use Xstream\MediaBundle\Entity\Media;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Xstream\MediaBundle\Model\Media\MediaFactory;
use Xstream\MediaBundle\Model\Media\SearchMediaQuery;
use Xstream\MediaBundle\Model\Media\UpdateMediaCommand;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class MediaController extends Controller
{
    /**
     * @ApiDoc(
     * description="Read single Media from library",
     * parameters={
     *  {"name" = "mediaId", "dataType"="integer", "required"=true, "description"="Id of media to get"}
     * },
     * statusCodes={
     *  404="Media not found"
     * }
     * )
     */
    public function getMediumAction($mediaId)
    {
        $mediaService = $this->get('model.media.service');

        return $mediaService->readMediaFromLibrary($mediaId);
    }

    /**
     * @ApiDoc(
     *  description="Browse Media library",
     *  statusCodes={
     *   400="Incorrect filter parameters"
     *  }
     * )
     *
     * @QueryParam(name="sort", nullable=true, description="Property used for sorting results")
     * @QueryParam(name="order", nullable=true, description="asc or desc order of soring, required when sort is set")
     * @QueryParam(name="limit", nullable=true, default=8, description="Number of results per page, max value 20")
     * @QueryParam(name="offset", nullable=true, default=0, description="Offset for browsing query")
     * @View(serializerGroups={"Default"})
     */
    public function getMediaAction(ParamFetcher $paramFetcher)
    {
        $searchMediaQuery = new SearchMediaQuery($paramFetcher->all());
        $mediaService = $this->get('model.media.service');

        return $mediaService->searchMediaInLibrary($searchMediaQuery);
    }

    /**
     * @ApiDoc(
     *  description="Adds new Media to library",
     *  input="Xstream\MediaBundle\Entity\Media",
     *  parameters={
     *   {"name" = "mediaId", "dataType"="integer", "required"=true, "description"="Id of media to update"}
     *  },
     *  statusCodes={
     *   400="Incorrect input parameters",
     *   401="Incorrect session token"
     *  }
     * )
     *
     * @Post("/media")
     * @View(statusCode=201)
     * @ParamConverter("media", converter="fos_rest.request_body", class="Xstream\MediaBundle\Entity\Media")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function postMediaAction(Media $media)
    {
        $mediaFactory = new MediaFactory();
        $media = $mediaFactory->createMediaFromDeserializedMedia($media);

        $mediaService = $this->get('model.media.service');
        $mediaService->addMediaToLibrary($media);
    }

    /**
     * @ApiDoc(
     *  description="Updates Media in library",
     *  input="Xstream\MediaBundle\Model\Media\UpdateMediaCommand",
     *  statusCodes={
     *   400="Incorrect input parameters",
     *   401="Incorrect session token",
     *   404="Media not found"
     *  }
     * )
     *
     * @View(statusCode=204)
     * @ParamConverter("updateMediaCommand", converter="fos_rest.request_body", class="Xstream\MediaBundle\Model\Media\UpdateMediaCommand")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function putMediaAction($mediaId, UpdateMediaCommand $updateMediaCommand)
    {
        $mediaService = $this->get('model.media.service');
        $updateMediaCommand->setId($mediaId);
        $mediaService->updateMediaInLibrary($updateMediaCommand);
    }

    /**
     * @ApiDoc(
     *  description="Removes Media from library",
     *  parameters={
     *   {"name" = "mediaId", "dataType"="integer", "required"=true, "description"="Id of media to remove"}
     *  },
     *  statusCodes={
     *   400="Incorrect input parameters",
     *   401="Incorrect session token",
     *   404="Media not found"
     *  }
     * )
     *
     * @View(statusCode=204)
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function deleteMediaAction($mediaId)
    {
        $mediaService = $this->get('model.media.service');
        $mediaService->removeMediaFromLibrary($mediaId);
    }
}