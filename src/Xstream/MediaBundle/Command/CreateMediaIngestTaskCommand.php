<?php
namespace Xstream\MediaBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateMediaIngestTaskCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('media:ingest:createtask')
            ->setDescription('Creates new media ingest task')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $task = json_encode(['task' => 'create media ingest']);

        $this->getContainer()->get('old_sound_rabbit_mq.ingest_media_producer')->publish($task);

        $output->writeln('Message was published: <info>' . $task);
    }
}