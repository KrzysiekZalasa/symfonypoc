<?php
namespace Xstream\MediaBundle\Tests\Consumer;

use PhpAmqpLib\Message\AMQPMessage;
use Xstream\MediaBundle\Consumer\MediaIngestConsumer;

class MediaIngestConsumerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Checks if consumer returns true with proper response from IngestService
     */
    public function testExecute()
    {
        $mediaIngestService = $this->getMockMediaIngestService();
        $message = new AMQPMessage();
        $mediaIngestConsumer = new MediaIngestConsumer($mediaIngestService);
        $this->assertTrue($mediaIngestConsumer->execute($message));
    }

    /**
     * Prepares mock of MediaIngestService
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getMockMediaIngestService()
    {
        $mediaIngestService = $this->getMockBuilder('Xstream\MediaBundle\Model\Media\MediaIngestService')
            ->disableOriginalConstructor()
            ->getMock();
        $mediaIngestService->method('importIngestedMedia');

        return $mediaIngestService;
    }
}
