<?php
namespace Xstream\MediaBundle\Tests\Controller;

use ReflectionClass;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Xstream\MediaBundle\Entity\Image;
use Xstream\MediaBundle\Entity\Media;
use Xstream\MediaBundle\Entity\Stream;


class MediaControllerTest extends WebTestCase
{
    /**
     * Checks GET single Media positive scenario
     */
    public function testGetMediumAction()
    {
        $client = static::createClient();
        $media = $this->createMediaEntity(1);
        $client->getContainer()->set('model.media.service', $this->getMockMediaService('readMediaFromLibrary', $media));
        $client->request('GET', '/media/1');

        $response = json_decode($client->getResponse()->getContent());

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('Example movie 1', $response->title);
        $this->assertEquals('Awesome story 1', $response->description);
        $this->assertEquals(1, count($response->images));
        $this->assertEquals(1, count($response->streams));
        $this->assertEquals(1, count($response->_links));
    }

    /**
     * Checks GET Media collection positive scenario
     */
    public function testGetMediaAction()
    {
        $client = static::createClient();
        $client->getContainer()->set('model.media.service', $this->getMockMediaService('searchMediaInLibrary', $this->createMediaLibrary()));

        $client->request('GET', '/media?limit=8&offset=0&order=asc&sort=title');

        $response = json_decode($client->getResponse()->getContent());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('8', $response->limit);
        $this->assertEquals('0', $response->offset);
        $this->assertEquals('3', $response->total);
        $this->assertTrue(is_int(strpos($response->_links->self->href, '?limit=8&offset=0&order=asc&sort=title')));
        $this->assertEquals('3', count($response->media));
    }

    /**
     * Checks POST media positive scenario
     */
    public function testPostMediaAction()
    {
        $client = static::createClient();
        $client->getContainer()->set('model.media.service', $this->getMockMediaService('addMediaToLibrary', null));
        $client->request('POST', '/media', [], [],
            ['CONTENT_TYPE' => 'application/json', 'HTTP_sessiontoken' => 'supersecrettoken'],
            '{ "id": 1, "title": "Some title" }'
        );
        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    /**
     * Checks PUT Media positive scenario
     */
    public function testPutMediaAction()
    {
        $client = static::createClient();
        $client->getContainer()->set('model.media.service', $this->getMockMediaService('updateMediaInLibrary', null));
        $client->request('PUT', '/media/1', [], [],
            ['CONTENT_TYPE' => 'application/json', 'HTTP_sessiontoken' => 'supersecrettoken'],
            '{ "id": 1, "title": "Some title" }'
        );
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
    }

    /**
     * Checks DELETE media positive scenario
     */
    public function testDeleteMediaAction()
    {
        $client = static::createClient();
        $client->getContainer()->set('model.media.service', $this->getMockMediaService('removeMediaFromLibrary', null));
        $client->request('DELETE', '/media/1', [], [],
            ['CONTENT_TYPE' => 'application/json', 'HTTP_sessiontoken' => 'supersecrettoken']
        );
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
    }

    /**
     * Returns mock of MediaService
     *
     * @param $method Method which will be executed
     * @param null $return Expected return value
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getMockMediaService($method, $return = null)
    {
        $mediaService = $this->getMockBuilder('Xstream\MediaBundle\Model\Media\MediaService')
            ->disableOriginalConstructor()
            ->getMock();
        $mediaService->method($method)->willReturn($return);

        return $mediaService;
    }

    /**
     * Creates Media entity instance with id = $id
     *
     * @param $id Unique Id of entity, used for customizing parts of entity
     *
     * @return Media
     */
    protected function createMediaEntity($id)
    {
        $image = new Image();
        $image->setId($id);
        $image->setHeight(100);
        $image->setWidth(300);
        $image->setSrc("http://example.com/{$id}.jpg");


        $stream = new Stream();
        $stream->setId($id);
        $stream->setSrc("http://example.com/{$id}.mp4");
        $stream->getType('mp4');

        $media = new Media();
        $media->setId($id);
        $media->setTitle('Example movie ' . $id);
        $media->setDescription('Awesome story ' . $id);
        $media->setImages([$image]);
        $media->setStreams([$stream]);

        return $media;
    }

    /**
     * Creates MediaLibrary object
     *
     * @return MediaLibrary
     */
    protected function createMediaLibrary()
    {
        $reflection = new ReflectionClass('Xstream\MediaBundle\Entity\MediaLibrary');
        $mediaLibrary = $reflection->newInstanceWithoutConstructor();
        $mediaLibrary->setLimit(8);
        $mediaLibrary->setOffset(0);
        $mediaLibrary->setOrder('asc');
        $mediaLibrary->setTotal(3);
        $mediaLibrary->setSort('title');
        $mediaLibrary->setMedia([
            $this->createMediaEntity(1), $this->createMediaEntity(2), $this->createMediaEntity(3)
        ]);

        return $mediaLibrary;
    }
}
