<?php
namespace Xstream\MediaBundle\Tests\Model\Media;

use Xstream\MediaBundle\Entity\Image;
use Xstream\MediaBundle\Entity\Media;
use Xstream\MediaBundle\Entity\Stream;
use Xstream\MediaBundle\Model\Media\MediaFactory;

class MediaFactoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Checks if Images and Streams has proper media object inside
     */
    public function testCreateMediaFromDeserializedMedia()
    {
        $deserializedMedia = $this->getMedia(1,
            [
                $this->getImage(1), $this->getImage(2)
            ],
            [
                $this->getStream(123)
            ]
        );

        $mediaFactory = new MediaFactory();
        $media = $mediaFactory->createMediaFromDeserializedMedia($deserializedMedia);

        foreach ($media->getImages() as $image) {
            $this->assertEquals(1, $image->getMedia()->getId());
        }
        foreach ($media->getStreams() as $stream) {
            $this->assertEquals(1, $stream->getMedia()->getId());
        }
    }

    /**
     * Create Media entity
     *
     * @param $id
     * @param array $images
     * @param array $streams
     * @return Media
     */
    protected function getMedia($id, $images = [], $streams = [])
    {
        $media = new Media();
        $media->setId($id);
        $media->setTitle('Example movie ' . $id);
        $media->setDescription('Awesome story ' . $id);
        $media->setImages($images);
        $media->setStreams($streams);

        return $media;
    }

    /**
     * Create Image object
     *
     * @param $id
     * @return Image
     */
    protected function getImage($id)
    {
        $image = new Image();
        $image->setId($id);
        $image->setHeight(100);
        $image->setWidth(300);
        $image->setSrc("http://example.com/{$id}.jpg");

        return $image;
    }

    /**
     * Create Stream object
     *
     * @param $id
     * @return Stream
     */
    protected function getStream($id)
    {
        $stream = new Stream();
        $stream->setId($id);
        $stream->setSrc("http://example.com/{$id}.mp4");
        $stream->getType('mp4');

        return $stream;
    }
}
