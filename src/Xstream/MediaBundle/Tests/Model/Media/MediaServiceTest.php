<?php
namespace Xstream\MediaBundle\Tests\Model\Media;

use Symfony\Component\Validator\ValidatorBuilder;
use Xstream\MediaBundle\Entity\Media;
use Xstream\MediaBundle\Model\Media\MediaService;

class MediaServiceTest extends \PHPUnit_Framework_TestCase
{
    public function testAddMediaToLibrary()
    {
        $mediaService = new MediaService(
            $this->getMediaRepositoryMock(),
            (new ValidatorBuilder())->enableAnnotationMapping()->getValidator()
        );

        $this->markTestIncomplete('Validator cannot use doctrine.unique here');
    }

    protected function getMediaRepositoryMock($method = null, $result = null)
    {
        $mediaRepositoryMock = $this->getMockBuilder('Xstream\MediaBundle\Entity\MediaRepository')
            ->disableOriginalConstructor()
            ->getMock();
        if ($method) {
            $mediaRepositoryMock->method($method)->willReturn($result);
        }

        return $mediaRepositoryMock;
    }
}
