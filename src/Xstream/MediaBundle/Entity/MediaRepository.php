<?php

namespace Xstream\MediaBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Xstream\MediaBundle\Model\Media\SearchMediaQuery;

class MediaRepository extends EntityRepository
{
    /**
     * Stores Media in repository
     *
     * @param Media $media
     */
    public function persist(Media $media)
    {
        $this->getEntityManager()->persist($media);
        $this->getEntityManager()->flush();
    }

    /**
     * Removes Media with $mediaId
     *
     * @param $mediaId
     */
    public function remove($mediaId)
    {
        $this->getEntityManager()->remove($mediaId);
        $this->getEntityManager()->flush();
    }

    /**
     * Search in MediaRepository against SearchMediaQuery
     *
     * @param SearchMediaQuery $searchMediaQuery
     *
     * @return MediaLibrary
     */
    public function search(SearchMediaQuery $searchMediaQuery)
    {
        $queryBuilder = $this->createQueryBuilder('Media');
        if (!is_null($searchMediaQuery->getSort())) {
            $queryBuilder->orderBy("Media." . $searchMediaQuery->getSort(), $searchMediaQuery->getOrder());
        }

        $queryBuilder
            ->setFirstResult($searchMediaQuery->getOffset())
            ->setMaxResults($searchMediaQuery->getLimit())
        ;

        return new MediaLibrary(new Paginator($queryBuilder), $searchMediaQuery->getSort(), $searchMediaQuery->getOrder());
    }
}
