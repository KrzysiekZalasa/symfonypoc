<?php

namespace Xstream\MediaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Hateoas\Configuration\Annotation as Hateoas;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Media
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Xstream\MediaBundle\Entity\MediaRepository")
 * @UniqueEntity("externalId")
 * @Hateoas\Relation("self", href = @Hateoas\Route("get_medium", parameters = { "mediaId" = "expr(object.getId())" }, absolute=true))
 * @Serializer\AccessType("public_method")
 *
 */
class Media
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Type("integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Serializer\Type("string")
     */
    private $description;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="Image", mappedBy="media", cascade={"persist", "remove"})
     * @Serializer\Type("array<Xstream\MediaBundle\Entity\Image>")
     * @Assert\Collection()
     */
    private $images;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="Stream", mappedBy="media", cascade={"persist", "remove"})
     * @Serializer\Type("array<Xstream\MediaBundle\Entity\Stream>")
     * @Serializer\Groups({"details"})
     * @Assert\Collection()
     */
    private $streams;

    /**
     * @var string
     *
     * @ORM\Column(name="externalId", type="string", length=255, nullable=true, unique=true)
     * @Serializer\Exclude()
     */
    private $externalId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Media
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Media
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set images
     *
     * @param array $images
     * @return Media
     */
    public function setImages($images)
    {
        $this->images = $images;

        return $this;
    }

    /**
     * Get images
     *
     * @return array<Image>
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set streams
     *
     * @param array $streams
     * @return Media
     */
    public function setStreams($streams)
    {
        $this->streams = $streams;

        return $this;
    }

    /**
     * Get streams
     *
     * @return array<Stream>
     */
    public function getStreams()
    {
        return $this->streams;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }
}
