<?php
namespace Xstream\MediaBundle\Entity;

use Doctrine\ORM\Tools\Pagination\Paginator;
use JMS\Serializer\Annotation as Serializer;
use Hateoas\Configuration\Annotation as Hateoas;

/**
 * Class MediaLibrary
 * @Hateoas\Relation("self", href =
 *      @Hateoas\Route(
 *          "get_media",
 *          absolute=true,
 *          parameters={
 *              "limit" = "expr(object.getLimit())",
 *              "offset" = "expr(object.getOffset())",
 *              "order" = "expr(object.getOrder())",
 *              "sort" = "expr(object.getSort())"
 *          }
 *      )
 * )
 * @Serializer\AccessType("public_method")
 */
class MediaLibrary
{
    /**
     * @Serializer\Type("array<Xstream\MediaBundle\Entity\Media>")
     */
    protected $media = [];

    /**
     * @Serializer\Type("integer")
     */
    protected $limit;

    /**
     * @Serializer\Type("integer")
     */
    protected $offset;

    /**
     * @Serializer\Type("integer")
     */
    protected $total;

    /**
     * @Serializer\Exclude()
     */
    protected $sort;

    /**
     * @Serializer\Exclude()
     */
    protected $order;

    /**
     * @param Paginator $mediaPaginator Paginator which contains results of query to MediaRepository
     * @param $sort Sort field name (title, id)
     * @param $order Sort order (asc, desc)
     */
    public function __construct(Paginator $mediaPaginator, $sort, $order)
    {
        $this->limit = $mediaPaginator->getQuery()->getMaxResults();
        $this->offset = $mediaPaginator->getQuery()->getFirstResult();
        $this->total = $mediaPaginator->count();
        $this->sort = $sort;
        $this->order = $order;

        foreach($mediaPaginator as $media) {
            $this->media[] = $media;
        }
    }

    /**
     * @return mixed
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * @return mixed
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @return mixed
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @return mixed
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $media
     */
    public function setMedia($media)
    {
        $this->media = $media;
    }

    /**
     * @param mixed $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @param mixed $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @param mixed $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

}