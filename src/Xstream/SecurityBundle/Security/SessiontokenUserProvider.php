<?php
namespace Xstream\SecurityBundle\Security;

use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;

class SessiontokenUserProvider implements UserProviderInterface
{
    public function loadUserByUsername($username)
    {
        if ($username == 'Konrad') {
            return $this->getUser();
        }

        throw new UsernameNotFoundException();
    }

    public function getUserBySessiontoken($sessiontoken)
    {
        if ($sessiontoken == 'supersecrettoken') {
            return $this->getUser();
        }

        throw new UsernameNotFoundException();
    }

    public function refreshUser(UserInterface $user)
    {
        return $this->getUser();
    }

    public function supportsClass($class)
    {
        return 'Symfony\Component\Security\Core\User\User' === $class;
    }

    protected function getUser()
    {
        return new User(
            'Konrad',
            'Password100',
            ['ROLE_ADMIN']
        );
    }
}