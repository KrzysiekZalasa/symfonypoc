<?php
namespace Xstream\SecurityBundle\Security\Firewall;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\HttpUtils;
use Xstream\SecurityBundle\Security\Authentication\SessiontokenToken;
use Xstream\SecurityBundle\Security\Authentication\UsernameToken;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class SessiontokenListener implements ListenerInterface
{
    /**
     * @var TokenStorageInterface - stores Token during request
     */
    protected $tokenStorage;

    /**
     * @var AuthenticationManagerInterface - authenticates token
     */
    protected $authenticationManager;

    /**
     * @var HttpUtils - HTTP utility stuff
     */
    protected $httpUtils;

    /**
     * @param TokenStorageInterface $tokenStorage
     * @param AuthenticationManagerInterface $authenticationManager
     * @param HttpUtils $httpUtils
     */
    public function __construct(TokenStorageInterface $tokenStorage, AuthenticationManagerInterface $authenticationManager, HttpUtils $httpUtils)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
        $this->httpUtils = $httpUtils;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $token = $this->getToken($request);

        try {
            $authToken = $this->authenticationManager->authenticate($token);
            $this->tokenStorage->setToken($authToken);

            return;
        } catch (AuthenticationException $failed) {

        }

        $response = new Response();
        $response->setStatusCode(Response::HTTP_FORBIDDEN);
        $event->setResponse($response);
    }

    /**
     * Validates token related data in request and creates unauthenticated token from it
     *
     * @param Request $request
     * @return SessiontokenToken|UsernameToken
     *
     * @throws AuthenticationException Request data aren't correct for token processing
     */
    protected function getToken(Request $request)
    {
        $loginUrl = '/login';
        if ($this->httpUtils->checkRequestPath($request, $loginUrl)) {
            $body = json_decode($request->getContent());

            if (!isset($body->username, $body->password)) {
                throw new AuthenticationException("Incorrect login request format");
            }

            $usernameToken = new UsernameToken();
            $usernameToken->username = $body->username;
            $usernameToken->password = $body->password;

            return $usernameToken;
        }

        if (is_null($request->headers->get('sessiontoken'))) {
            throw new AuthenticationException("Missing session token parameter");
        }

        $sessiontoken = new SessiontokenToken();
        $sessiontoken->sessiontoken = $request->headers->get('sessiontoken');

        return $sessiontoken;
    }
}