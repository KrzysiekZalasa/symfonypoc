<?php
namespace Xstream\SecurityBundle\Security\Authentication;

use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UsernameProvider implements AuthenticationProviderInterface
{
    protected $userProvider;

    public function __construct(UserProviderInterface $userProvider)
    {
        $this->userProvider = $userProvider;
    }

    public function authenticate(TokenInterface $token)
    {
        $user = $this->userProvider->loadUserByUsername($token->username);

        if ($user->getPassword() === $token->password) {
            $authenticatedToken = new SessiontokenToken($user->getRoles());
            $authenticatedToken->setUser($user);
            $authenticatedToken->sessiontoken = $this->getRandomToken();

            return $authenticatedToken;
        }

        throw new AuthenticationException('Incorrect username or password');
    }

    public function supports(TokenInterface $token)
    {
        return $token instanceof UsernameToken;
    }

    protected function getRandomToken()
    {
        return 'supersecrettoken';
    }
}