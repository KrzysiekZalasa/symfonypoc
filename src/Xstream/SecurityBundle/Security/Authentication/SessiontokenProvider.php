<?php
namespace Xstream\SecurityBundle\Security\Authentication;

use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserProviderInterface;


class SessiontokenProvider implements AuthenticationProviderInterface
{
    protected $userProvider;

    public function __construct(UserProviderInterface $userProvider)
    {
        $this->userProvider = $userProvider;
    }

    public function authenticate(TokenInterface $token)
    {
        try {
            $user = $this->userProvider->getUserBySessiontoken($token->sessiontoken);
        } catch (UsernameNotFoundException $e) {
            throw new AuthenticationException("Incorrect session token");
        }

        $authenticatedToken = new SessiontokenToken($user->getRoles());
        $authenticatedToken->setUser($user);
        $authenticatedToken->sessiontoken = $token->sessiontoken;

        return $authenticatedToken;
    }

    public function supports(TokenInterface $token)
    {
        return $token instanceof SessiontokenToken;
    }
}