<?php

namespace Xstream\SecurityBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    /**
     * Checks POST login positive scenario
     */
    public function testLoginAction()
    {
        $client = static::createClient();
        $client->request('POST', '/login', [], [],
            ['CONTENT_TYPE' => 'application/json'],
            '{ "username": "Konrad", "password": "Password100" }'
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
