<?php

namespace Xstream\SecurityBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Xstream\SecurityBundle\DependencyInjection\SessiontokenFactory;
use Xstream\SecurityBundle\DependencyInjection\UsernameFactory;

class XstreamSecurityBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new SessiontokenFactory());
        $extension->addSecurityListenerFactory(new UsernameFactory());
    }

}
