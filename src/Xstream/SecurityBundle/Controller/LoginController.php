<?php

namespace Xstream\SecurityBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\Post;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class LoginController extends Controller
{
    /**
     * @ApiDoc(
     *  description="Log in user to system",
     *  parameters={
     *   {"name"="username", "dataType"="string", "required"=true, "description"="Login"},
     *   {"name"="password", "dataType"="string", "required"=true, "description"="Password"},
     * },
     *  statusCodes={
     *   401="Incorrect login or password"
     * }
     * )
     *
     * @Post("/login")
     */
    public function loginAction()
    {
        $token = $this->get('security.context')->getToken();

        return ['token' => $token->sessiontoken];
    }
}
